package lab10.ex3;

public class Counter extends Thread {

    Thread t;
    static int var = 0;

    Counter(String name, Thread t){
        super(name);
        this.t = t;
    }

    public void run(){

        if(t != null) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for(int i=0;i<100;i++){
            System.out.println(getName() + " i = "+i);
            var++;
        }
        System.out.println(getName() + " job finalised." + var);
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("counter1", null);
        Counter c2 = new Counter("counter2", c1);

        c1.start();
        c2.start();
    }
}

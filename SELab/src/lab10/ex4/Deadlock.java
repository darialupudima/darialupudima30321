package lab10.ex4;


import java.util.ArrayList;
import java.util.Random;

public class Deadlock {

    public static void main(String[] args) {
        final ArrayList<Robot> robots = new ArrayList<>();

        for(int i=1; i<=10; i++) {
            Robot robot = new Robot("Robot " + i);
            robots.add(robot);
        }

        while(true)
        {
            
        }

    }
}

class Point {
    int x, y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point getXY() {
        return new Point(x, y);
    }
}

class Robot {
    private final String name;

    
    Random random = new Random();
    volatile Point location = new Point(random.nextInt(101), random.nextInt(101));

    public Robot(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

    public void move()
    {
        int x,y;
        do {
            x = random.nextInt(3) - 1;
            y = random.nextInt(3) - 1;
        }while (location.x + x > 100 || location.x + x < 0 || location.y + y > 100 || location.y + y < 0);

        this.location =  new Point(location.x + x, location.y+ y);

    }
}
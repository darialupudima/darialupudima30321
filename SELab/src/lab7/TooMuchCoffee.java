package lab7;


public class TooMuchCoffee extends Exception {

    public TooMuchCoffee(String msg)
    {
        super(msg);
    }
}

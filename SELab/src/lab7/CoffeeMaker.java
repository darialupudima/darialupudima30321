package lab7;


class CoffeeMaker {
    private static int instances = 0;
    private static int maxInstances;

    Coffee makeCoffee() throws TooMuchCoffee{
        System.out.println("Make a coffee.");

        instances++;
        if(instances > maxInstances) throw new TooMuchCoffee("Too many cups of coffee were created.");

        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        return new Coffee(t,c);
    }

    public static void getMaxInstances(int max)
    {
        maxInstances = max;
    }
}
package lab7;

import java.io.*;
import java.util.Scanner;

public class CharCountFromFile {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Give character to search for : ");
        char character = scanner.next().charAt(0);
        int nr = 0;

        try{
            BufferedReader br = new BufferedReader(new FileReader("src\\lab7\\data.txt"));

            int c;
            while( (c = br.read()) != -1 )
                if(c == character) nr++;

            br.close();
        }catch(Exception ex)
        {
            System.out.println("The file has not been found!");
            System.exit(1);
        }

        System.out.println("The character " + character + " was found: " + nr + " times.");

    }

}

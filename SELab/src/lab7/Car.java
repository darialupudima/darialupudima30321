package lab7;

import java.io.*;
import java.util.Objects;

public class Car implements Serializable {

    private String model;
    private transient double price;

    public Car(String model, double price)
    {
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model = " + model +
                ", price = " + price +
                '}';
    }

    public void saveToFile() throws IOException {
        ObjectOutputStream outStream = new ObjectOutputStream(new FileOutputStream("src\\Lab7\\Car.bin", true));
        outStream.writeObject(this);
        outStream.close();
    }

    public static void readCar(String model) throws IOException, ClassNotFoundException {
        ObjectInputStream inStream = new ObjectInputStream(new FileInputStream("src\\Lab7\\Car.bin"));
        Car car = (Car) inStream.readObject();

        if(model.equals(car.model)) {
            System.out.println(car);
        }
        inStream.close();
    }

    public static void readAllCars() throws IOException, ClassNotFoundException {
        ObjectInputStream inStream = new ObjectInputStream(new FileInputStream("src\\Lab7\\Car.bin"));

        Car car;
        try {
            while ((car = (Car) inStream.readObject()) != null)
                System.out.println(car);
        }
        catch (EOFException e)
        {
            inStream.close();
        }
    }
}

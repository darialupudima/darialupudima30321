package lab7;


import java.util.Scanner;

public class CoffeeTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int maxInstances;
        System.out.print("Give the maximum number of coffee cups to create : ");
        maxInstances = scanner.nextInt();
        CoffeeMaker.getMaxInstances(maxInstances);

        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();

        for(int i = 0; i<15; i++){
            Coffee c = new Coffee(0,0);
            try {
                 c = mk.makeCoffee();
            }
            catch (TooMuchCoffee e)
            {
                System.out.println("Exception : " + e.getMessage());
                System.exit(1);
            }

            try {
                d.drinkCoffee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
            }
            finally{
                System.out.println("Throw the coffee cup.\n");
            }
        }
    }
}
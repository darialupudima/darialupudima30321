package lab7;


class CoffeeDrinker{
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"The offee is too hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"The offee concentration is too high!");
        System.out.println("Drink coffee:" + c);
    }
}
package lab7;


import java.io.*;
import java.util.Scanner;

public class EncryptedFile {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String choice = "";

        do {

            do {
                System.out.println("\nOptions : encrypt/decrypt/exit.");
                System.out.print("Your choice : ");
                choice = scanner.next(); // choice = arg[0]
            }while(choice.compareTo("encrypt") != 0 && choice.compareTo("decrypt") != 0 && choice.compareTo("exit") != 0);

            if(choice.compareTo("exit") == 0)
            {
                System.out.println("Program has been terminated.");
                System.exit(0);
            }

            System.out.print("Give the name of the file : ");
            String readFile = scanner.next(); // readFile = arg[1]


            String ext = "";
            if(choice.compareTo("encrypt") == 0) ext = ".enc";
            else if(choice.compareTo("decrypt") == 0) ext = ".dec";

            File file = new File(readFile);
            String writeFile = readFile.substring(0, readFile.lastIndexOf('.')) + ext;

            try {
                BufferedReader br = new BufferedReader(new FileReader("src\\lab7\\" + readFile));
                BufferedWriter bw = new BufferedWriter(new FileWriter("src\\lab7\\" + writeFile));

                int c = 0;
                while ((c = br.read()) != -1) {
                    if(choice.compareTo("encrypt") == 0) bw.write(c - 1);
                    else if(choice.compareTo("decrypt") == 0) bw.write(c + 1);
                }

                br.close();
                bw.close();
            } catch (Exception ex) {
                System.out.println("File has not been found.");
                System.exit(1);
            }

        }while(true);

    }

}

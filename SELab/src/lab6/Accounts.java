package lab6;

public class Accounts extends BankAccount implements Comparable<Accounts>{

    public Accounts(String name, double initialDeposit)
    {
        super(name, initialDeposit);
    }

    @Override
    public int compareTo(Accounts o) {
        Accounts acc = (Accounts) o;
        return this.getOwner().compareTo(acc.getOwner());
    }
}

package lab6;

import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        Dictionary dict = new Dictionary();
        char ans = '1';
        String word, definition;

        do {
            System.out.println("\nMenu");
            System.out.println("1 - Add Word");
            System.out.println("2 - Get Definition");
            System.out.println("3 - Get All Words");
            System.out.println("4 - Get All Definitions");
            System.out.println("e - Exit");

            System.out.print("Your option : ");
            ans = scanner.next().charAt(0);

            switch (ans) {
                case '1' -> {
                    System.out.print("\nGive the word : ");
                    word = scanner.next();
                    scanner.nextLine();
                    if (word.length() > 1) {
                        System.out.print("Give the definition : ");
                        definition = scanner.nextLine();
                        dict.addWord(word, definition);
                    }
                }
                case '2' -> {
                    System.out.print("\nSearch Word : ");
                    word = scanner.next();
                    definition = null;
                    if (word.length() > 1) {
                        definition = dict.getDefinition(word);
                        if (definition == null)
                            System.out.println("\nThe word doesn't exist.");
                        else
                            System.out.println("\nDefiniton : " + definition);
                    }
                }
                case '3' -> dict.getAllWords();
                case '4' -> dict.getAllDefinitions();
            }
        } while(ans != 'e' && ans != 'E');
        System.out.println("\nProgram has been terminated.");
    }
}

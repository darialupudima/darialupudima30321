package lab6;


import java.util.*;

public class Bank2 {
    private TreeSet<Accounts> accounts = new TreeSet<>();

    public void addAccount(Accounts acc)
    {
        accounts.add(acc);
    }

    public void removeAccount(Accounts acc)
    {
        accounts.remove(acc);
    }

    public void printAccounts()
    {
        ArrayList<Accounts> copy = new ArrayList<>(accounts);

        for(int i=0; i<copy.size()-1; i++)
            for(int j=i+1; j<copy.size(); j++)
                if(copy.get(i).getBalance() > copy.get(j).getBalance())
                    Collections.swap(copy, i, j);

        for(Accounts acc : copy)
            System.out.println("The owner of the account is " + acc.getOwner() + " and the account balance is " + acc.getBalance());
    }

    public void printAccounts(double minbalance, double maxbalance)
    {
        for(Accounts acc : accounts)
            if(acc.getBalance() >= minbalance && acc.getBalance() <= maxbalance)
                System.out.println("The owner of the account is " + acc.getOwner() + " and the account balance is " + acc.getBalance());
    }

    public Accounts getAccount(String owner)
    {
        for(Accounts acc : accounts)
            if(acc.getOwner().equals(owner))
                return acc;
        return null;
    }

    public TreeSet<Accounts> getAllAccounts()
    {
        return accounts;
    }
}

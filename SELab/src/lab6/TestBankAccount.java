package lab6;

public class TestBankAccount {
    public static void main(String[] args) {
        BankAccount account = new BankAccount("Michael", 1000);
        Object obj = new BankAccount("Michael", 1000);

        if(account.equals(obj) && account.hashCode() == obj.hashCode())
            System.out.println("The objects are the same");
        else System.out.println("The objects are not the same");

    }
}

package lab6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Bank {
    private ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(BankAccount acc)
    {
        accounts.add(acc);
    }

    public void removeAccount(BankAccount acc)
    {
        accounts.remove(acc);
    }

    public void printAccounts()
    {
        ArrayList<BankAccount> copy = accounts;

        for(int i=0; i<copy.size()-1; i++)
            for(int j=i+1; j<copy.size(); j++)
                if(copy.get(i).getBalance() > copy.get(j).getBalance())
                    Collections.swap(copy, i, j);

        for(BankAccount acc : copy)
            System.out.println("The owner of the account is " + acc.getOwner() + " and the account balance is " + acc.getBalance());
    }

    public void printAccounts(double minbalance, double maxbalance)
    {
        for(BankAccount acc : accounts)
            if(acc.getBalance() >= minbalance && acc.getBalance() <= maxbalance)
            System.out.println("The owner of the account is " + acc.getOwner() + " and the account balance is " + acc.getBalance());
    }

    public BankAccount getAccount(String owner)
    {
        for(BankAccount acc : accounts)
            if(acc.getOwner().equals(owner))
                return acc;
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts()
    {
        return accounts;
    }

    public void SortAlphabetically()
    {
        for(int i=0; i<accounts.size()-1; i++)
            for(int j=i+1; j<accounts.size(); j++)
                if(accounts.get(i).getOwner().compareTo(accounts.get(j).getOwner()) > 0)
                    Collections.swap(accounts, i, j);
    }
}

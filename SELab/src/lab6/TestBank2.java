package lab6;


public class TestBank2 {
    public static void main(String[] args) {
        Bank2 bank = new Bank2();

        for(int i=0; i<5; i++)
        {
            Accounts acc = new Accounts((5-i) + "Michael", 1000 - i*100);
            bank.addAccount(acc);
        }

        System.out.println("\nThe bank accounts sorted alphabetically after owner name : ");
        for(BankAccount acc : bank.getAllAccounts())
            System.out.println(acc.getOwner());

    }
}

package lab6;


import java.util.HashMap;

public class Dictionary {

    private HashMap<String, String> dictionary = new HashMap<>();

    public void addWord(String word, String definition)
    {
        if(dictionary.containsKey(word))
            System.out.println("The word already exists in the dictionary.");
        else
        {
            System.out.println("The word has been added to the dictionary.");
            dictionary.put(word, definition);
        }
    }

    public String getDefinition(String word)
    {
        return dictionary.get(word);
    }

    public void getAllWords()
    {
        System.out.print("\nHere are all the words from the dictionary : ");
        for(String key : dictionary.keySet())
            System.out.print(key + ", ");
        System.out.println();
    }

    public void getAllDefinitions()
    {
        System.out.println("\nHere are all the definitions from the dictionary : ");
        for(String key : dictionary.keySet())
            System.out.println(key + " : " + dictionary.get(key));
    }
}

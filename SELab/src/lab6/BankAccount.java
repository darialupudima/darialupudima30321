package lab6;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String name, double initialDeposit)
    {
        owner = name;
        balance = initialDeposit;
    }

    public void withdraw(double amount)
    {
        if(amount > balance)
            System.out.println("Insufficient Funds!");
        else balance -= amount;
    }

    public void deposit(double amount)
    {
        balance += amount;
    }

    public double getBalance()
    {
        return balance;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(obj instanceof BankAccount)
            return owner.equals(((BankAccount) obj).owner) && balance == ((BankAccount) obj).balance;
        else return false;
    }

    @Override
    public int hashCode()
    {
        int hash = 17;
        hash = 31 * hash + owner.hashCode();
        hash = 31 * hash + (int)balance;
        return hash;
    }

    public String getOwner() {
        return owner;
    }
}

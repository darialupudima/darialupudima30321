package lab6;


public class TestBank {
    public static void main(String[] args) {
        Bank bank = new Bank();

        for(int i=0; i<5; i++)
        {
            BankAccount acc = new BankAccount((5-i) + "Michael", 1000 - i*100);
            bank.addAccount(acc);
        }

        System.out.println("\nThe bank accounts before sorting : ");
        for(BankAccount acc : bank.getAllAccounts())
            System.out.println(acc.getOwner());

        bank.SortAlphabetically();

        System.out.println("\nThe bank accounts after being sorted alphabetically after owner name : ");
        for(BankAccount acc : bank.getAllAccounts())
            System.out.println(acc.getOwner());

    }
}

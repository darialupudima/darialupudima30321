package lab4;

import lab3.Circle;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder()
    {
        super(1, "red");   ///se apeleaza doar in constructor 
        					///mereu se foloseste in constr pt apelarea constr din SUPERCLASA
        					/// e primul lurcru pe care il faci<3
        					/// aici musai trebuie folosit dar se foloseste si in alte lpocuri pt apelarea metodelor din superclasa
    };

    public Cylinder(double radius)
    {
        super(radius, "red");
    }

    public Cylinder(double radius, double height)
    {
        super(radius, "red");
        this.height = height;
    }

    public double getHeight()
    {
        return height;
    }

    public double getVolume()
    {
        return super.getArea() * height;
    }

    @Override
    public double getArea()
    {
        return 2 * Math.PI * this.getRadius() * (this.getRadius() + height);
    }
}

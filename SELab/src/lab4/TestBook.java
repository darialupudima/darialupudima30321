package lab4;

import lab3.Author;

import java.util.Arrays;

public class TestBook {
    public static void main(String[] args) {
        Author[] authors = new Author[3];

        for(int i=1; i<=3; i++)
        {
            authors[i-1] = new Author("Author" + i, "email" + i, i%2 == 0 ? 'm' : 'f');
        }

        Book book = new Book("Harry Potter", authors, 10.50);

        book.setQtyInStock(50);

        System.out.println("A book was created.\nIts name is " + book.getName() + "\nIts authors are :\n" + Arrays.toString(book.getAuthors()) + "\nIts price is $" + book.getPrice() + "\nIts quantity in stock is " + book.getQtyInStock());

        System.out.println("\n" + book);
        book.printAuthors();
    }
}

package lab4;
import lab3.Author;


import java.util.Arrays;

public class Book {
    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;


    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors()
    {
        System.out.println(Arrays.toString(authors));
    }

    @Override
    public String toString()
    {
        return this.name + " by " + authors.length + " authors";
    }
}

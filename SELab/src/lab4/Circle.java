package lab4;

public class Circle extends Shape {
    public double getRadius()
    {
        return radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    private double radius;

    public Circle()
    {
        super();
        this.radius = 1.0;
    };

    public Circle(double r)
    {
        super();
        this.radius = r;
    }

    public Circle(double r, String color, boolean filled)
    {
        super(color, filled);
        this.radius = r;
    }

    public double getArea()
    {
        return Math.PI * radius * radius;
    }

    public double getPerimeter()
    {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString()
    {
        return "A Circle with radius=" + radius + ", which is a subclass of " + super.toString();
    }

}


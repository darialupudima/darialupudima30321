package lab4;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder cylinder = new Cylinder(1, 2);

        System.out.println("A cylinder was created with the radius " + cylinder.getRadius() + " and the height " + cylinder.getHeight()
        + ". Its surface area is " + cylinder.getArea() + " and its volume is " + cylinder.getVolume());
    }
}

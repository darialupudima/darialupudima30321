package lab3;

public class Flower {
    int petal;
    static int flowerNo;
    Flower(){
        System.out.println("Flower has been created!");
        flowerNo++;
    }
    static void printFlowerNo()
    {
        System.out.println("flower number: " + flowerNo);
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
            printFlowerNo();
        }
    }
}

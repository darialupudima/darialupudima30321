package lab3;

public class TestCircle {

    public static void main(String[] args) {
        Circle c1 = new Circle(5);
        Circle c2 = new Circle(7,"blue");
        System.out.println("the area is " + c2.getArea());
        System.out.println("the radius is " + c1.getRadius());

    }
}

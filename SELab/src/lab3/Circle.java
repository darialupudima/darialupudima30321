package lab3;

public class Circle {
	    private double radius = 1.0;
	    private String color = "red";

	    Circle(double a){
	        radius = a;
	    }
	    public Circle(double a, String b){   ///aici era protected
	        radius = a;
	        color = b;
	    }
	    public double getRadius(){
	        return radius;
	    }
	    public double getArea(){
	        return radius * radius * Math.PI;
	    }

	
}

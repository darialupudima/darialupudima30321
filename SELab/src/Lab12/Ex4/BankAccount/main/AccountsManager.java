package Lab12.Ex4.BankAccount.main;

import java.util.ArrayList;
import java.util.Objects;

public class AccountsManager {

    ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(BankAccount account)
    {
        accounts.add(account);
    }

    public boolean exists(String id){
        for(BankAccount acc : accounts)
            if(Objects.equals(acc.getId(), id))
                return true;
        return false;
    }

    public int count()
    {
        return accounts.toArray().length;
    }
}

package Lab12.Ex3.Vehicle2.main;

import java.util.Objects;

public class Vehicle {

    private String type;

    private int weight;
    public Vehicle(String type, int length) {
        this.type = type;
    }

    public String start(){
        return "engine started";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lab12.Ex3.Vehicle2.main.Vehicle vehicle = (Lab12.Ex3.Vehicle2.main.Vehicle) o;
        return weight == vehicle.weight &&
                type.equals(vehicle.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, weight);
    }


    public String getType() {
        return type;
    }

    public int getWeight() {
        return weight;
    }

}

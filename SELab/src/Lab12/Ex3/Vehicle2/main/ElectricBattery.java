package Lab12.Ex3.Vehicle2.main;

public class ElectricBattery {

    /**
     * Percentage load.
     */
    private int charge = 0;

    public void charge() throws BatteryException {
        if(charge >= 100)
            throw new BatteryException("Battery is already full.");
        else charge++;
    }

}

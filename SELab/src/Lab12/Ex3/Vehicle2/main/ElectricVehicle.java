package Lab12.Ex3.Vehicle2.main;

public class ElectricVehicle extends Vehicle {
    public ElectricVehicle(String type, int length) {
        super(type, length);
    }

    @Override
    public String start(){
        return "electric engine started";
    }
}


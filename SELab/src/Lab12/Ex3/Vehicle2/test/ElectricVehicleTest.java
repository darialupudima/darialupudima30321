package Lab12.Ex3.Vehicle2.test;

import Lab12.Ex3.Vehicle2.main.ElectricVehicle;
import org.junit.Test;

import static org.junit.Assert.*;

public class ElectricVehicleTest {

    @Test
    public void testStart(){
        ElectricVehicle x = new ElectricVehicle("DSCF34", 1100);
        assertEquals("electric engine started", x.start());
    }

}
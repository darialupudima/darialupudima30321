package Lab12.Ex2.Vehicles.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Parking {

    /**
     * Vehicles will be parked in parkedVehicles array.
     */
    ArrayList<Vehicle> parkedVehicles = new ArrayList<>();

    public void parkVehicle(Vehicle e){
        parkedVehicles.add(e);
    }

    /**
     * Sort vehicles by weight.
     */
    public void sortByWeight(){
        parkedVehicles.sort(new CustomComparator());
    }

    public Vehicle get(int index){
        return parkedVehicles.get(index);
    }

    public static class CustomComparator implements Comparator<Vehicle> {
        @Override
        public int compare(Vehicle o1, Vehicle o2) {
            return o1.getWeight() - o2.getWeight();
        }
    }

}

package lab11.ex2;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Store extends JFrame implements Observer {

    ArrayList<Product> products;
    JButton addProductButton;
    JButton deleteProductButton;
    JButton viewProductsButton;
    JButton changeProductQuantityButton;

    Store()
    {
        setTitle("Store");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500,500);
        setLocation(700, 300);
        setVisible(true);
        this.setLayout(null);

        addProductButton = new JButton("Add Product");
        addProductButton.setBounds(10, 10, 150, 50);
        add(addProductButton);


        products = new ArrayList<>();
    }

    public void addProduct()
    {

    }

    public void deleteProduct()
    {

    }

    public void viewProducts()
    {

    }

    public void changeProductQuantity()
    {

    }

    @Override
    public void update(Observable o, Object arg) {

    }

    public static void main(String[] args) {
        Store store = new Store();
    }
}

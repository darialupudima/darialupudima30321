package lab11.ex1;


import java.util.Observable;
import java.util.Random;

public class TemperatureSensor extends Observable implements Runnable{

    private int value;

    public int getValue()
    {
        return value;
    }

    TemperatureSensor()
    {
        readTemperature();
    }

    public void readTemperature()
    {
        Random random = new Random();
        setChanged();
        notifyObservers(value);
        value = random.nextInt(100);
    }

    @Override
    public void run() {
        System.out.println("New Thread has been started.");
    }

    public static void main(String[] args) throws InterruptedException {
        TemperatureSensor sensor = new TemperatureSensor();
        UserInterface watcher = new UserInterface();
        sensor.addObserver(watcher);
        Thread thread = new Thread(sensor);
        thread.start();

        while(true)
        {
            Thread.sleep(1000);
            sensor.readTemperature();
        }
    }
}

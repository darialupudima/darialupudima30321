package lab11.ex1;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class UserInterface extends JFrame implements Observer{

    JTextField textField = new JTextField("Temperature: 0");

    UserInterface()
    {
        setTitle("Temperature Sensor");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(200,200);
        setLocation(700, 300);
        setVisible(true);
        textField.setBounds(20, 50, 150, 50);
        add(textField);

        this.setLayout(null);
    }

    @Override
    public void update(Observable obj, Object arg) {
        int temperature = ((TemperatureSensor)obj).getValue();
        textField.setText("Temperature: " + temperature);
    }
}
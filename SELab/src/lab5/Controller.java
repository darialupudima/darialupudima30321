package lab5;
import lab5.TemperatureSensor;  

import lab5.TemperatureSensor;  

public class Controller {
    TemperatureSensor tempSensor = new TemperatureSensor();
    LightSensor lightSensor = new LightSensor();

    private static Controller controller;
    private Controller() {}
    public static Controller getInstance()
    {
        if(controller == null)
            controller = new Controller();
        return controller;
    }

    public void control()
    {
        for(int i=1; i<=20; i++)
        {
            System.out.println("The temperature at second " + i + " is " + tempSensor.readValue() + " and the light value is " + lightSensor.readValue());
        }
    }
}

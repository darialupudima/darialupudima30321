package lab5;

public class ProxyImage implements Image {

	private RotatedImage rotatedImage;
    private RealImage realImage;
    private String fileName;
    private boolean displayRealImage;

    public ProxyImage(String fileName, boolean displayRealImage) {
        this.fileName = fileName;
        this.displayRealImage = displayRealImage;
    }

    @Override
    public void display() {
        if (realImage == null) {
            realImage = new RealImage(fileName);
        }
        if(displayRealImage)
            realImage.display();
        else rotatedImage.display();
    }

    @Override
    public void RotatedImage()
    {
        System.out.println("Display rotated " + fileName);
    }
}

package lab5;

public class Circle extends Shape {
    protected double radius;

    public Circle()
    {
        super();
        radius = 1.0;
    };

    public Circle(double r)
    {
        super();
        radius = r;
    }

    public Circle(double r, String color, boolean filled)
    {
        super(color, filled);
        radius = r;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
    public double getRadius()
    {
        return radius;
    }

    @Override
    public double getArea()
    {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter()
    {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString()
    {
        return "A Circle with radius=" + radius + ", which is a subclass of " + super.toString();
    }

}

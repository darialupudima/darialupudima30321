package lab5;

import java.util.Random;

public class LightSensor extends Sensor{

    @Override
    public int readValue()
    {
        Random rand = new Random();
        return rand.nextInt(100);
    }
}

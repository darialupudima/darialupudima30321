package lab5;

public class TestShapes {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];

        shapes[0] = new Circle(2);
        shapes[1] = new Rectangle(1, 2);
        shapes[2] = new Square(2);

        for(int i=0; i<3; i++)
            System.out.println("Shape " + i + " has area = " + shapes[i].getArea() + " and perimeter = " + shapes[i].getPerimeter());
    }
}


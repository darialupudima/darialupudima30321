package Lab2;

import java.util.Scanner;

public class Ex6 {

    //recursive method
    static int factorial(int number) {
        if(number >= 1) return factorial(number-1) * number;
        else return 1;
    }

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Please enter the number: ");
        int N = in.nextInt();

        // non recursive method
        int result = 1;
        for(int i = 1; i<=N; i++) {
            result = result * i;
        }
        System.out.println("The non recursive method is: " + result);
        int recursiveResult;
        recursiveResult = factorial(N);
        System.out.println("The recursive method is: " + recursiveResult);

    }

}

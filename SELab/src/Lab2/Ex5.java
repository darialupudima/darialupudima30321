package Lab2;

import java.util.Random;

public class Ex5 {
    public static void main(String[] args) {

        Random rand = new Random();

        int []vector = new int[11];
        for(int i = 0; i < 10; i++) {
            vector[i] = rand.nextInt(1000); // generates random numbers between 0 and 999
        }
        for(int i=0; i<10; i++) {
            System.out.print(vector[i]+ " ");
        }
        System.out.println();

        // bubble sort
        for(int i=0; i<10; i++){
            for(int j=i+1; j<10; j++) {
                if(vector[i] > vector[j]) {
                    int aux = vector[i];
                    vector[i] = vector[j];
                    vector[j] = aux;
                }
            }
        }

        for(int i=0; i<10; i++) {
            System.out.print(vector[i]+ " ");
        }



    }
}
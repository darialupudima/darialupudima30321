package Lab2;

import java.util.Scanner;

public class Ex4 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);   //il folosesti peste tot
		int n, i;
		int maxx = 0;
		System.out.print("Enter n: "); 
	    n = scan.nextInt(); 
		
		int []a = new int[n];
		for(i = 0; i < n; i++)
		{
			a[i] = scan.nextInt(); 
			if(a[i] > maxx)
				maxx = a[i];
		}
			
			scan.skip("\n"); // printeaza dupa enter
		  System.out.println("\nMaximum = " +maxx);
	}

}

package Lab2;

import java.util.Scanner;

public class Ex3 {
	public static void main(String[] args) {
		 Scanner scan = new Scanner(System.in); 
         
        int A, B, i, j, flag;
        
        System.out.printf("Enter lower bound of the interval: ");
        A = scan.nextInt(); // Take input
 
        System.out.printf("\nEnter upper bound of the interval: ");
        B = scan.nextInt(); // Take input
 
        
      
        for (i = A; i <= B; i++) {
 
            if (i == 1 || i == 0)
                continue;
 
            // flag variable to tell if i is prime or not
            flag = 1;
 
            for (j = 2; j <= i / 2; ++j) {
                if (i % j == 0) {
                    flag = 0;
                    break;
                }
            }
 
            // flag = 1 means i is prime and flag = 0 means i is not prime
            if (flag == 1)
                System.out.println(i);
	}

}

}
package Lab2;

	import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args)  {

        System.out.println("Enter a number between 1 and 9: ");
        Scanner scan = new Scanner(System.in); 

        int Number = scan.nextInt();

        switch (Number){

        	case 0: 


            case 1:
                System.out.println("ONE");
                break;
            case 2:
                System.out.println("TWO");
                break;
            case 3:
                System.out.println("THREE");
                break;
            case 4:
                System.out.println("FOUR");
                break;
            case 5:
                System.out.println("FIVE");
                break;
            case 6:
                System.out.println("SIX");
                break;
            case 7:
                System.out.println("SEVEN");
                break;
            case 8:
                System.out.println("EIGHT");
                break;
            case 9:
                System.out.println("NINE");
                break;
            default:
                System.out.println("OTHER");
                
                
                ///or with if
                if(Number == 0)
                	System.out.println("ZERO");
                else if (Number == 1)
                	System.out.println("ONE");
                else if (Number == 2)
                	System.out.println("TWO");
                else if (Number == 3)
                	System.out.println("THREE");
                else if (Number == 4)
                	System.out.println("FOUR");
                else if (Number == 5)
                	System.out.println("FIVE");
                else if (Number == 6)
                	System.out.println("SIX");
                else if (Number == 7)
                	System.out.println("SEVEN");
                else if (Number == 8)
                	System.out.println("EIGHT");
                else if (Number == 9)
                	System.out.println("NINE");
                else
                	 System.out.println("OTHER");
                	
                
        }
    }
}

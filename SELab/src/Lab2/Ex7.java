package Lab2;

import java.util.Random;
import java.util.Scanner;

//ex 7
public class Ex7 {

    static void guessGame(){
        Random rand = new Random();
        Scanner in = new Scanner(System.in);
        int theNumber, guessedNumber;
        theNumber = rand.nextInt(11)+ 1; // i added 1 to avoid '0'

        System.out.println("Try to guess the number. It is between 0 and 10");
        for(int i=0; i<3; i++) {
            guessedNumber = in.nextInt();
            if(guessedNumber == theNumber) {
                System.out.println("You won!");
                return;
            } else if(guessedNumber < theNumber) {
                System.out.println("Wrong answer, your number it too low!");
            } else {
                System.out.println("Wrong answer, your number it too high!");
            }
        }
        System.out.println("You lost! The number was " + theNumber);


    }

    public static void main(String[] args) {
        guessGame();

    }

}
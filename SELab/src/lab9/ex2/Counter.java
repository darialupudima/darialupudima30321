package lab9.ex2;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {
    JTextField text = new JTextField();
    JButton button = new JButton("CLICK ME");
    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            nr++;
            Counter.this.text.setText(String.valueOf(nr));
        }
    };


    int nr = 0;

    public Counter()
    {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,350);
        setLocation(600, 100);
        setVisible(true);


        this.setLayout(null);
        int width=200; int height = 100;

        text.setBounds(100,50,width, height/2);
        add(text);
        Counter.this.text.setText(String.valueOf(nr));

        button.setBounds(100, 150, width, height);
        add(button);
        button.addActionListener(listener);
    }

    public static void main(String[] args) {
        new Counter();
    }
}
package lab9.ex5;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Railway extends JFrame {

    static JTextArea info1;
    static JTextArea info2;
    static JTextArea info3;
    JLabel tinfo1, tinfo2, tinfo3, turda, ias, timisoara;
    JLabel text1, text2, text3;
    JLabel nameofTrain1, nameofTrain2, nameofTrain3;
    JLabel segmentParked1, segmentParked2, segmentParked3;
    JButton add1, add2, add3;
    JTextField destination1, destination2, destination3;
    JTextField segment1, segment2, segment3;
    JLabel name1,name2,name3;
    JTextField data1,data2,data3;

    Railway() {
        setTitle("Railway Traffic");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(700, 700);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);

        turda = new JLabel("Turda ");
        turda.setBounds(10, 10, 100, 30);
        add(turda);

        tinfo1 = new JLabel("Traffic Info: ");
        tinfo1.setBounds(10, 30, 100, 30);
        add(tinfo1);

        info1 = new JTextArea();
        info1.setBounds(10, 50, 300, 150);
        add(info1);

        text1 = new JLabel("Add train");
        text1.setBounds(320, 50, 100, 30);
        add(text1);

        name1=new JLabel("Name:");
        name1.setBounds(320, 70, 100, 30);
        add(name1);

        data1=new JTextField();
        data1.setBounds(320, 100, 100, 20);
        add(data1);

        nameofTrain1 = new JLabel("Destination:");
        nameofTrain1.setBounds(420, 70, 100, 30);
        add(nameofTrain1);

        destination1 = new JTextField();
        destination1.setBounds(420, 100, 100, 20);
        add(destination1);

        segmentParked1 = new JLabel(" Parked:");
        segmentParked1.setBounds(320, 110, 100, 30);
        add(segmentParked1);

        segment1 = new JTextField();
        segment1.setBounds(320, 130, 100, 20);
        add(segment1);

        add1 = new JButton("Add");
        add1.setBounds(320, 150, 100, 50);
        add1.addActionListener(new Action());
        add(add1);

        ias = new JLabel("Ias ");
        ias.setBounds(10, 210, 100, 30);
        add(ias);

        tinfo2 = new JLabel("Traffic Info ");
        tinfo2.setBounds(10, 230, 100, 30);
        add(tinfo2);

        info2 = new JTextArea();
        info2.setBounds(10, 250, 300, 150);
        add(info2);

        text2 = new JLabel("Add train");
        text2.setBounds(320, 250, 100, 30);
        add(text2);

        name2 = new JLabel("Name:");
        name2.setBounds(320, 260, 100, 30);
        add(name2);

        data2 = new JTextField();
        data2.setBounds(320, 285, 100, 20);
        add(data2);


        nameofTrain2 = new JLabel("Destination:");
        nameofTrain2.setBounds(420, 260, 100, 30);
        add(nameofTrain2);

        destination2 = new JTextField();
        destination2.setBounds(420, 285, 100, 20);
        add(destination2);

        segmentParked2 = new JLabel("Parked:");
        segmentParked2.setBounds(320, 295, 100, 30);
        add(segmentParked2);

        segment2 = new JTextField();
        segment2.setBounds(320, 320, 100, 20);
        add(segment2);

        add2 = new JButton("Add");
        add2.setBounds(320, 340, 100, 50);
        add2.addActionListener(new Action());
        add(add2);

        timisoara = new JLabel("Timisoara");
        timisoara.setBounds(10, 410, 100, 30);
        add(timisoara);

        tinfo3 = new JLabel("Traffic Info ");
        tinfo3.setBounds(10, 430, 100, 30);
        add(tinfo3);

        info3 = new JTextArea();
        info3.setBounds(10, 450, 300, 150);
        add(info3);

        text3 = new JLabel("Add train");
        text3.setBounds(320, 450, 100, 30);
        add(text3);

        name3 = new JLabel("Name:");
        name3.setBounds(320, 460, 100, 30);
        add(name3);

        data3= new JTextField();
        data3.setBounds(320, 485, 100, 20);
        add(data3);

        nameofTrain3 = new JLabel("Destination:");
        nameofTrain3.setBounds(420, 460, 100, 30);
        add(nameofTrain3);

        destination3 = new JTextField();
        destination3.setBounds(420, 485, 100, 20);
        add(destination3);

        segmentParked3 = new JLabel("Parked:");
        segmentParked3.setBounds(320, 495, 100, 30);
        add(segmentParked3);

        segment3 = new JTextField();
        segment3.setBounds(320, 520, 100, 20);
        add(segment3);

        add3 = new JButton("Add");
        add3.setBounds(320, 540, 100, 50);
        add3.addActionListener(new Action());
        add(add3);


    }

    public static void main(String[] args) {
        new Railway();


        //build station Turda
        Controller c1 = new Controller("Turda");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        Controller c2 = new Controller("Iasi");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        Controller c3 = new Controller("Timisoara");

        Segment s10 = new Segment(10);
        Segment s11 = new Segment(11);
        Segment s12 = new Segment(12);

        c3.addControlledSegment(s10);
        c3.addControlledSegment(s11);
        c3.addControlledSegment(s12);


        //testing

        Train t1 = new Train("Turda", "IC-001");
        s4.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca", "R-002");

        for(Segment s:c1.list){
            if (s.hasTrain())
                info1.append("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|"+"\n");
            else
                info1.append("|----------ID=" + s.id + "__Train=______ catre ________----------|"+"\n");
        }
        for(Segment s:c2.list){
            if (s.hasTrain())
                info2.append("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|"+"\n");
            else
                info2.append("|----------ID=" + s.id + "__Train=______ catre ________----------|"+"\n");
        }
        for(Segment s:c3.list){
            if (s.hasTrain())
                info3.append("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|"+"\n");
            else
                info3.append("|----------ID=" + s.id + "__Train=______ catre ________----------|"+"\n");
        }

    }
    public class Action implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==add1){
                Train t = new Train(Railway.this.destination1.getText(), Railway.this.data1.getText());
                Segment s=new Segment(Integer.parseInt(Railway.this.segment1.getText()));
                s.arriveTrain(t);
                info1.append("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|"+"\n");

            }
            if(e.getSource()==add2){
                Train t = new Train(Railway.this.destination2.getText(), Railway.this.data2.getText());
                Segment s=new Segment(Integer.parseInt(Railway.this.segment2.getText()));
                s.arriveTrain(t);
                info2.append("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|"+"\n");

            }
            if(e.getSource()==add3){
                Train t = new Train(Railway.this.destination3.getText(), Railway.this.data3.getText());
                Segment s=new Segment(Integer.parseInt(Railway.this.segment3.getText()));
                s.arriveTrain(t);
                info1.append("|----------ID=" + s.id + "__Train=" + s.getTrain().name + " to " + s.getTrain().destination + "__----------|"+"\n");

            }

        }
    }
}
package lab9.ex3;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

public class OpenFileWButton extends JFrame{
    JTextArea ta;
    JTextField name = new JTextField();
    JButton button = new JButton("Open File");
    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                String fileName = OpenFileWButton.this.name.getText();
                open(fileName);
            }catch(Exception ignored){}
        }
    };

    OpenFileWButton()
    {
        setTitle("Press button to open file");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600,350);
        setLocation(600, 100);
        setVisible(true);

        this.setLayout(null);
        int width=200;int height = 100;

        name.setBounds(50,50,width, height/2);
        add(name);

        button.setBounds(50, 150, width, height);
        add(button);
        button.addActionListener(listener);

        ta = new JTextArea();
        ta.setBounds(300, 50, 250, 200);
        ta.setLineWrap(true);
        add(ta);
    }

    void open(String f)
    {
        try
        {
            ta.setText("");
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader(f));
            String l = "";
            l = bf.readLine();
            while (l!=null)
            {
                ta.append(l+"\n");
                l = bf.readLine();
            }
            bf.close();
        }catch(Exception ex){System.out.println("File could not be opened.");}
    }

    public static void main(String[] args) {
        new OpenFileWButton();
    }
}


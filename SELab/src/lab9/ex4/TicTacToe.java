package lab9.ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class TicTacToe extends JFrame{

    private boolean turnX = true;
    JButton[][] buttons = new JButton[3][3];
    ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton clicked = (JButton) e.getSource();

            if(Objects.equals(clicked.getText(), "")) {
                if (turnX) {
                    clicked.setText("X");
                    turnX = false;
                } else {
                    clicked.setText("O");
                    turnX = true;
                }

                String winner = checkWinner();
                if(!Objects.equals(winner, "None"))
                {
                    // Game is OVER
                    displayWinner(winner);
                }
            }
        }
    };

    TicTacToe()
    {
        setTitle("Tic Tac Toe Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400,400);
        setLocation(600, 100);
        setVisible(true);

        this.setLayout(null);

        //Instantiate buttons
        for(int i=0; i<3; i++)
            for(int j=0; j<3; j++)
            {
                buttons[i][j] = new JButton("");
                buttons[i][j].setBounds(40+i*100, 20+j*100, 100, 100);
                buttons[i][j].addActionListener(listener);
                add(buttons[i][j]);
            }
    }

    private String checkWinner()
    {
        // Line
        for(int i=0; i<3; i++)
            if(Objects.equals(buttons[i][0].getText(), buttons[i][1].getText()) && Objects.equals(buttons[i][1].getText(), buttons[i][2].getText()) && !buttons[i][1].getText().isEmpty()
            || Objects.equals(buttons[0][i].getText(), buttons[1][i].getText()) && Objects.equals(buttons[1][0].getText(), buttons[2][i].getText()) && !buttons[1][0].getText().isEmpty())
            {
                return buttons[i][i].getText();
            }

        // Diagonal
        if(Objects.equals(buttons[0][0].getText(), buttons[1][1].getText()) && Objects.equals(buttons[1][1].getText(), buttons[2][2].getText()) && !buttons[1][1].getText().isEmpty()
        || Objects.equals(buttons[0][2].getText(), buttons[1][1].getText()) && Objects.equals(buttons[1][1].getText(), buttons[2][0].getText()) && !buttons[1][1].getText().isEmpty())
        {
            return buttons[1][1].getText();
        }

        return "None";
    }

    private void displayWinner(String winner)
    {
        // Remove all buttons. For some reason it throws an error
        //removeAll();
        JTextField message = new JTextField();
        message.setText("The Winner is " + winner);
        message.setBounds(100, 100, 200, 50);
        add(message);
    }

    public static void main(String[] args) {
        TicTacToe game = new TicTacToe();
    }
}

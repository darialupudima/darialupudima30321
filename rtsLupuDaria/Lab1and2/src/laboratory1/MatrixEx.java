package laboratory1;

public class MatrixEx{

    public static void main(String[] args) {

        // cresating the first matrix
        int[][] matrix1 = {{2, 3, 1}, {7, 1, 6}, {9, 2, 4}};

        // creating the second matrix
        int[][] matrix2 = {{8, 5, 3}, {3, 9, 2}, {2, 7, 3}};

        // sum of the matrices
        int[][] sumMatrix = sumMatrices(matrix1, matrix2);

        System.out.println("Sum of matrices:");
        printMatrix(sumMatrix);

        // product of the matrices
        int[][] productMatrix = productMatrices(matrix1, matrix2);

        System.out.println("Product of matrices:");
        printMatrix(productMatrix);
    }

    public static int[][] sumMatrices(int[][] matrix1, int[][] matrix2) {

        int[][] result = new int[matrix1.length][matrix1[0].length];

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1[0].length; j++) {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }

        return result;
    }

    public static int[][] productMatrices(int[][] matrix1, int[][] matrix2) {

        int[][] result = new int[matrix1.length][matrix2[0].length];

        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix2[0].length; j++) {
                for (int k = 0; k < matrix1[0].length; k++) {
                    result[i][j] += matrix1[i][k] * matrix2[k][j];
                }
            }
        }

        return result;
    }

    public static void printMatrix(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println();
    }

}
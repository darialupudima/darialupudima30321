package laboratory1;

public class ComplexNumber {
    private double realNr;
    private double imagNr;

    
    public ComplexNumber(double real, double imag) {
        this.realNr = real;
        this.imagNr = imag;
    }

    public ComplexNumber add(ComplexNumber other) {
        double newReal = this.realNr + other.realNr;
        double newImag = this.imagNr + other.imagNr;
        return new ComplexNumber(newReal, newImag);
    }

    public ComplexNumber multiply(ComplexNumber other) {
        double newReal = (this.realNr * other.realNr) - (this.imagNr * other.imagNr);
        double newImag = (this.realNr * other.imagNr) + (this.imagNr * other.realNr);
        return new ComplexNumber(newReal, newImag);
    }

    public String toString() {
        return String.format("%.2f + %.2fi", realNr, imagNr);
    }

    public static void main(String[] args) {
        ComplexNumber num1 = new ComplexNumber(2, 5);
        ComplexNumber num2 = new ComplexNumber(4, -1);

        ComplexNumber sum = num1.add(num2);
        ComplexNumber product = num1.multiply(num2);

        System.out.println("First number: " + num1);
        System.out.println("Second number: " + num2);
        System.out.println("Sum: " + sum);
        System.out.println("Product: " + product);
    }
}